The original file was taken from here:
https://www.gnu.org/graphics/skwetu-gnu-logo.html

The original file was created by Skwetu and is available under the terms of the Free Art License v1.3.

I removed the background and if that gives me any kind of copyright on the file, I provide it here under the terms of the Free Art License v1.3.
